#include <gtest/gtest.h>
#include "material.h"

class LYSOTest : public ::testing::Test{
public:
    void SetUp() override{
        lyso = new Material("LYSO_atten_coeff.txt", 7.25);
    }
    void TearDown( ) { 
        delete lyso;
   }
    Material* lyso;
};


TEST_F(LYSOTest, getTotalAttenCoeff)
{
    double energy(0.01), expectedAtten(1.686E+02*7.25);
    double interpolatedAtten = lyso->getTotalAttenCoeff(energy);
    // EXPECT_DOUBLE_EQ(interpolatedAtten, expectedAtten);
    EXPECT_NEAR(interpolatedAtten, expectedAtten, 0.01*expectedAtten);

    energy = 0.999;
    expectedAtten = ((0.99-0.8)/0.2*6.432E-02+(1-0.99)/0.2*7.617E-02)*7.25;
    interpolatedAtten = lyso->getTotalAttenCoeff(energy);
    // EXPECT_DOUBLE_EQ(interpolatedAtten, expectedAtten);
    EXPECT_NEAR(interpolatedAtten, expectedAtten, 0.01*expectedAtten);

    energy = 1.000E-01; 
    expectedAtten = 3.032E+00*7.25;
    interpolatedAtten = lyso->getTotalAttenCoeff(energy);
    // EXPECT_DOUBLE_EQ(interpolatedAtten, expectedAtten);
    EXPECT_NEAR(interpolatedAtten, expectedAtten, 0.01*expectedAtten);
}

TEST_F(LYSOTest,getCSProbability)
{
    double energy(0.01), expectedProb(0.00040709373532811684);
    double interpolatedProb = lyso->getComptonScatteringProbability(energy);
    EXPECT_NEAR(interpolatedProb, expectedProb, 0.01*expectedProb);

    energy = 0.999;
    expectedProb = 0.8677659540472309;
    interpolatedProb = lyso->getComptonScatteringProbability(energy);
    EXPECT_NEAR(interpolatedProb, expectedProb, 0.01*expectedProb);

    energy = 1.000E-01;
    expectedProb = 0.039170427532986524;
    interpolatedProb = lyso->getComptonScatteringProbability(energy);
    EXPECT_NEAR(interpolatedProb, expectedProb, 0.01*expectedProb);
}
