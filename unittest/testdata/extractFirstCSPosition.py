'''
Description: 
Author: Ming Fang
Date: 2022-10-04 17:50:13
LastEditors: Ming Fang mingf2@illinois.edu
LastEditTime: 2022-10-05 21:30:46
'''
from pathlib import Path
import numpy as np


def extractFirstCSPosition(fpath):
    positions = []
    with open(fpath, 'r') as f:
        prevHistNum = -1
        for line in f:
            currHistNum = int(line[:15])
            if prevHistNum == currHistNum:
                continue
            interactionType = int(line[25:28])
            if interactionType != 1:
                continue
            initEnergy = float(line[133:142])
            if abs(initEnergy-0.661) > 1e-3:
                continue
            substrs = line[84:107].split()
            position = np.array([float(s) for s in substrs])
            positions.append(position)
            prevHistNum = currHistNum
    return positions


simulation_output_dir = Path('/media/ming/DATA/projects/Postprocessing/simulation_09212022')
angles = np.array([(0, 0), (60, 0), (90, 0), (120, 0), (150, 0), (180, 0), 
                       (0, 30), (0, 60), (90, 30), (90, 60)])
for azimuth, elevation in angles:
    print(azimuth, elevation)
    if elevation == 0:
        dir_path = Path(simulation_output_dir/(f"1/azimuth_{azimuth}"))
    else:
        dir_path = Path(simulation_output_dir/(f"2/azimuth_{azimuth}_elevation_{elevation}"))
    positions = extractFirstCSPosition(dir_path/"comptonImager0.d")
    np.savetxt(f"tests/testdata/MCNPR1_azimuth_{azimuth}_elevation_{elevation}.txt", positions, fmt='%.2f')

