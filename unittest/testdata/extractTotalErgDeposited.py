'''
Description: Extract total energy deposited in a series of two events.
Author: Ming Fang
Date: 2022-10-05 20:12:21
LastEditors: Ming Fang mingf2@illinois.edu
LastEditTime: 2022-10-06 00:20:10
'''
from pathlib import Path
import numpy as np


def extractTotalEnergy2(fpath):
    totalErgs = []
    with open(fpath, 'r') as f:
        lines = f.readlines()
        lino = 0
        prevHistNum = -1
        savePrev = False
        prevParNum = 0
        skipHistory = False
        while lino < len(lines):
            line = lines[lino]
            lino += 1
            currHistNum = int(line[:15])
            currParNum = int(line[15:19])
            # if new history
            if currHistNum != prevHistNum:
                if savePrev:
                    totalErgs.append([totalErg, prevHistNum])
                savePrev = False
                prevHistNum = currHistNum
                prevParNum = currParNum
                eventNumber = 0
                # first interaction must be CS
                interactionType = int(line[25:28])
                if interactionType != 1:
                    skipHistory = True
                    continue
                else:
                    skipHistory = False
                initEnergy = float(line[133:142])
                if abs(initEnergy-0.661) > 1e-3:
                    continue
                substrs = line[84:107].split()
                prevPos = np.array([float(s) for s in substrs])
                firstPos = prevPos
                firstCell = int(line[38:45])
                prevScatterNum = int(line[125:130])
                totalErg = float(line[45:57])
            # same history
            elif skipHistory:
                continue
            elif currParNum == 1:
                # first particle
                substrs = line[84:107].split()
                currPos = np.array([float(s) for s in substrs])
                interactionType = int(line[25:28])
                currScatterNum = int(line[125:130])
                if currScatterNum!=prevScatterNum:
                    if eventNumber == 0:
                        secondPos = currPos
                        secondCell = int(line[38:45])
                    # this is a new event
                    prevPos = currPos
                    prevScatterNum = currScatterNum
                    eventNumber += 1
                if eventNumber > 1:
                    continue
                else:
                    totalErg += float(line[45:57])
                if eventNumber >= 1:
                    savePrev = True
            else:
                # new particel in same history
                substrs = line[84:107].split()
                currPos = np.array([float(s) for s in substrs])
                currCell = int(line[38:45])
                # if np.linalg.norm(currPos - firstPos) < 0.05:
                #     totalErg += float(line[45:57])
                # elif eventNumber >= 1 and np.linalg.norm(currPos - secondPos) < 0.05:
                #     totalErg += float(line[45:57])
                if currCell == firstCell:
                    totalErg += float(line[45:57])
                elif eventNumber >= 1 and currCell == secondCell:
                    totalErg += float(line[45:57])
        if savePrev:
            totalErgs.append([totalErg, prevHistNum])  

    return totalErgs

def extractTotalEnergy(fpath):
    totalErgs = []
    with open(fpath, 'r') as f:
        prevHistNum = -1
        eventNumber = 0
        savePrev = False
        for line in f:
            currHistNum = int(line[:15])
            if prevHistNum == currHistNum:
                if skipHistory:
                    continue
                substrs = line[84:107].split()
                currPos = np.array([float(s) for s in substrs])
                interactionType = int(line[25:28])
                currScatterNum = int(line[125:130])
                if currScatterNum!=prevScatterNum or np.linalg.norm(currPos - prevPos) > 0.1:
                    # this is a new event
                    prevPos = currPos
                    prevScatterNum = currScatterNum
                    eventNumber += 1
                if eventNumber > 1:
                    skipHistory = True
                    continue
                else:
                    totalErg += float(line[45:57])
                if eventNumber >= 1:
                    savePrev = True
            else:
                if savePrev:
                    totalErgs.append([totalErg, prevHistNum])
                savePrev = False
                prevHistNum = currHistNum
                eventNumber = 0
                # first interaction must be CS
                interactionType = int(line[25:28])
                if interactionType != 1:
                    skipHistory = True
                    continue
                else:
                    skipHistory = False
                initEnergy = float(line[133:142])
                if abs(initEnergy-0.661) > 1e-3:
                    continue
                substrs = line[84:107].split()
                prevPos = np.array([float(s) for s in substrs])
                prevScatterNum = int(line[125:130])
                totalErg = float(line[45:57])
        if savePrev:
            totalErgs.append([totalErg, prevHistNum])
    return totalErgs


simulation_output_dir = Path('/media/ming/DATA/projects/Postprocessing/simulation_09212022')
angles = np.array([(0, 0), (60, 0), (90, 0), (120, 0), (150, 0), (180, 0), 
                       (0, 30), (0, 60), (90, 30), (90, 60)])
for azimuth, elevation in angles:
    print(azimuth, elevation)
    if elevation == 0:
        dir_path = Path(simulation_output_dir/(f"1/azimuth_{azimuth}"))
    else:
        dir_path = Path(simulation_output_dir/(f"2/azimuth_{azimuth}_elevation_{elevation}"))
    energies = extractTotalEnergy2(dir_path/"comptonImager0.d")
    np.savetxt(f"tests/testdata/MCNPTotErg_azimuth_{azimuth}_elevation_{elevation}.txt", energies, fmt='%.6f %d')

