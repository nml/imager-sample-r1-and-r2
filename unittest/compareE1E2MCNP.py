import numpy as np
import sys
from matplotlib import pyplot as plt
plt.style.use(['science','grid','high-vis'])


def compareE1E2toMCNP(i):
    # phi, theta
    azimuth, elevation = angles[i]

    nbins = 100
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))

    data = np.loadtxt(f'unittest/testdata/sampled_r1r2E1E2_azimuth_{azimuth}_elevation_{elevation}.txt', skiprows=1, delimiter='\t')
    totalErgDeposited = data[:, -1] + data[:, -2]
    n = len(totalErgDeposited)
    counts, binEdges = np.histogram(totalErgDeposited, range=(0, 1), bins=nbins)
    ax.step((binEdges[:-1]+binEdges[1:])/2, counts/n, where='mid', label='Sampled')
    # print(np.sum(counts/n))

    data = np.loadtxt(f"unittest/testdata/MCNPTotErg_azimuth_{azimuth}_elevation_{elevation}.txt", skiprows=1)
    totalErgDepositedMCNP = data[:, 0]
    n = len(totalErgDepositedMCNP)
    counts, binEdges = np.histogram(totalErgDepositedMCNP, range=(0, 1), bins=nbins)
    ax.step((binEdges[:-1]+binEdges[1:])/2, counts/n, where='mid', label='MCNP')
    # print(np.sum(counts/n))

    ax.legend()
    ax.set_xlabel("Total energy deposited (MeV)")
    ax.set_ylabel("Probability")
    fig.savefig(f"unittest/testdata/total_energy_comparison_azimuth_{azimuth}_elevation_{elevation}.png")
    plt.show()


if __name__ == '__main__':
    case_id = int(sys.argv[1])
    print(f"Plot total deposited energy comparison for case {case_id}")
    # phi, theta
    angles = [(0, 0), (60, 0), (90, 0), (120, 0), (150, 0), (180, 0), 
                       (0, 30), (0, 60), (90, 30), (90, 60)]
    compareE1E2toMCNP(case_id)
