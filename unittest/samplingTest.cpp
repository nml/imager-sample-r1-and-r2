#include <iostream>
#include <cmath>
#include <chrono>
#include "sampling.h"

int main(int argc, char** argv)
{
    const std::vector<std::pair<int, int>> angles{{0, 0}, {60, 0}, {90, 0}, {120, 0}, {150, 0}, {180, 0}, 
                       {0, 30}, {0, 60}, {90, 30}, {90, 60}};
    const double k(M_PI/180);
    const double E0(0.6617);
    Vector3D r0;

    const int nSample(100000);
    const int numWorkers(10); // number of cores. serial mode if set to 1.

    double total_time(0);
    for (int case_id = 0; case_id < angles.size(); case_id++)
    {
        // std::cout << "Sampling coincidence events for case " << case_id << std::endl;

        std::vector<TwoEvents> sampled_r1r2E1E2;

        // std::cout << angles[case_id].first << ", " << angles[case_id].second << std::endl;
        double azimuth = angles[case_id].first*k;
        double elevation = angles[case_id].second*k;
        r0 = 30 * Vector3D(std::cos(elevation)*std::cos(azimuth), std::cos(elevation)*std::sin(azimuth), std::sin(elevation));

        auto startTime = std::chrono::high_resolution_clock::now();
        sampled_r1r2E1E2 = run(r0, E0, nSample, numWorkers);
        auto endTime = std::chrono::high_resolution_clock::now();
        auto case_i_time = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
        std::cout << "Case (" << angles[case_id].first << ", " << angles[case_id].second << "), "<< nSample << " samples, " << numWorkers << " threads, time = " << case_i_time << "ms" << std::endl;
        total_time += case_i_time;

        std::string outputname = "/media/ming/DATA/projects/sampler1r2/unittest/testdata/sampled_r1r2E1E2_azimuth_";
        outputname = outputname + std::to_string(int(angles[case_id].first))+"_elevation_"+std::to_string(angles[case_id].second) + ".txt";
        saveEvents2txt(outputname, sampled_r1r2E1E2);
    }
    std::cout << angles.size() << " cases, average time per case = " << double(total_time)/angles.size() << "ms" << std::endl;

    return 0;

    // compare with mcnp using python
}