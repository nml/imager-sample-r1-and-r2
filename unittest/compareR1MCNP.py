import numpy as np
import sys
from matplotlib import pyplot as plt
plt.style.use(['science','grid','high-vis'])


def compareR1toMCNP(i):
    xBins = 150
    yBins = 150
    zBins = 100

    # phi, theta
    azimuth, elevation = angles[i]

    firstInteractionPositions = np.loadtxt(f"unittest/testdata/sampled_r1r2E1E2_azimuth_{azimuth}_elevation_{elevation}.txt", skiprows=1, delimiter='\t')
    n = len(firstInteractionPositions)

    fig, axes = plt.subplots(1, 3, figsize=(18,6))
    ax1, ax2, ax3 = axes
    counts, binEdges = np.histogram(firstInteractionPositions[:,0], range=(-2.1, 2.1), bins=xBins)
    ax1.step((binEdges[:-1]+binEdges[1:])/2, counts/n, where='mid', label='Sampled')
    counts, binEdges = np.histogram(firstInteractionPositions[:,1], range=(-3.45, 3.45), bins=yBins)
    ax2.step((binEdges[:-1]+binEdges[1:])/2, counts/n, where='mid', label='Sampled')
    counts, binEdges = np.histogram(firstInteractionPositions[:,2], range=(-2.5, 2.5), bins=zBins)
    ax3.step((binEdges[:-1]+binEdges[1:])/2, counts/n, where='mid', label='Sampled')

    firstInteractionPositions = np.loadtxt(f"unittest/testdata/MCNPR1_azimuth_{azimuth}_elevation_{elevation}.txt")
    n = len(firstInteractionPositions)

    counts, binEdges = np.histogram(firstInteractionPositions[:,0], range=(-2.1, 2.1), bins=xBins)
    ax1.step((binEdges[:-1]+binEdges[1:])/2, counts/n, where='mid', label='MCNP')
    counts, binEdges = np.histogram(firstInteractionPositions[:,1], range=(-3.45, 3.45), bins=yBins)
    ax2.step((binEdges[:-1]+binEdges[1:])/2, counts/n, where='mid', label='MCNP')
    counts, binEdges = np.histogram(firstInteractionPositions[:,2], range=(-2.5, 2.5), bins=zBins)
    ax3.step((binEdges[:-1]+binEdges[1:])/2, counts/n, where='mid', label='MCNP')

    ax1.set_xlabel("X (cm)")
    ax1.set_ylabel("Probability")
    ax1.legend()
    ax2.set_xlabel("Y (cm)")
    ax2.set_ylabel("Probability")
    ax2.legend()
    ax3.set_xlabel("Z (cm)")
    ax3.set_ylabel("Probability")
    ax3.legend()

    fig.savefig(f"unittest/testdata/R1_comparison_azimuth_{azimuth}_elevation_{elevation}.png")

    plt.show()


if __name__ == '__main__':
    case_id = int(sys.argv[1])
    print(f"Plot R1 comparison for case {case_id}")
    angles = [(0, 0), (60, 0), (90, 0), (120, 0), (150, 0), (180, 0), 
                       (0, 30), (0, 60), (90, 30), (90, 60)]
    compareR1toMCNP(case_id)

