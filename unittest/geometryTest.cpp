#include <gtest/gtest.h>
#include "geometry.h"

class GeometryTest : public ::testing::Test{
public:
    void SetUp() override{
        crystals = new Crystals();
    }
    void TearDown( ) { 
        delete crystals;
   }
    Crystals* crystals;
};

TEST_F(GeometryTest, checkIntersectBoundingBox)
{
    Vector3D origin(0,0,0), direction(0,0,1);
    EXPECT_TRUE(crystals->_checkIntersectBoundingBox(Ray(origin, direction)));

    origin = Vector3D(-30,0,0);
    direction = Vector3D(1,0,0);
    EXPECT_TRUE(crystals->_checkIntersectBoundingBox(Ray(origin, direction)));

    origin = Vector3D(-30,0,10);
    direction = Vector3D(1,0,0);
    EXPECT_FALSE(crystals->_checkIntersectBoundingBox(Ray(origin, direction)));

    origin = Vector3D(30,0,0);
    direction = Vector3D(1,0,0);
    EXPECT_FALSE(crystals->_checkIntersectBoundingBox(Ray(origin, direction)));

    origin = Vector3D(0,30,0);
    direction = Vector3D(0,-1,0);
    EXPECT_TRUE(crystals->_checkIntersectBoundingBox(Ray(origin, direction)));

    origin = Vector3D(0,30,10);
    direction = Vector3D(0,-1,0);
    EXPECT_FALSE(crystals->_checkIntersectBoundingBox(Ray(origin, direction)));

    origin = Vector3D(0,30,0);
    direction = Vector3D(0,1,0);
    EXPECT_FALSE(crystals->_checkIntersectBoundingBox(Ray(origin, direction)));

    origin = Vector3D(10,10,0);
    direction = Vector3D(-1,-1,0);
    EXPECT_TRUE(crystals->_checkIntersectBoundingBox(Ray(origin, direction)));
}

TEST_F(GeometryTest, getXRayIntersectionLength)
{
    Vector3D origin(30,0,1), direction(-1,0,0);
    double expectedIntersectionLength = 4*0.3;
    double intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_NEAR(intersectionLength, expectedIntersectionLength, 1e-6*expectedIntersectionLength);

    origin = Vector3D(30,0,10);
    expectedIntersectionLength = 0;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(30,0.55, 1);
    expectedIntersectionLength = 0;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);
}

TEST_F(GeometryTest, getYRayIntersectionLength)
{
    Vector3D origin(0,-30,1), direction(0,1,0);
    double expectedIntersectionLength = 0;
    double intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(0,30,10);
    direction = Vector3D(0,-1,0);
    expectedIntersectionLength = 0;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(0.65,30, 1);
    expectedIntersectionLength = 7*0.3;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_NEAR(intersectionLength, expectedIntersectionLength, 1e-6*expectedIntersectionLength);
    // EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);
}

TEST_F(GeometryTest, getZRayIntersectionLength)
{
    Vector3D origin(0,0,-10), direction(0,0,1);
    double expectedIntersectionLength = 0;
    double intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(0.65,0,10);
    direction = Vector3D(0,0,-1);
    expectedIntersectionLength = 5;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(0.65,0.55,10);
    direction = Vector3D(0,0,1);
    expectedIntersectionLength = 0;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(0.65,0,1);
    direction = Vector3D(0,0,1);
    expectedIntersectionLength = 2.5 - origin.z();
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(0.65,0,1);
    direction = Vector3D(0,0,-1);
    expectedIntersectionLength = origin.z() + 2.5;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);
}

TEST_F(GeometryTest, getRayIntersectionLength)
{
    Vector3D origin(0,0,0), direction(-1.95,-3.3,0.1);
    double expectedIntersectionLength = 0.697161;
    double intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_NEAR(intersectionLength, expectedIntersectionLength, 1e-6*expectedIntersectionLength);
    // EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(0,0,0);
    direction = Vector3D(-1.95,-3.3,5);
    expectedIntersectionLength = 0.5727453099612816;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(10,10,0);
    direction = Vector3D(1.95-10, 3.3-10, 1.5-0);
    expectedIntersectionLength = 1.514412264569012;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_NEAR(intersectionLength, expectedIntersectionLength, 1e-6*expectedIntersectionLength);
    // EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    origin = Vector3D(30,0,10);
    direction = Vector3D(1.95-30, 3.3-0, 1.5-10);
    expectedIntersectionLength = 0.446890722350209;
    intersectionLength = crystals->getRayIntersectionLength(Ray(origin, direction));
    EXPECT_NEAR(intersectionLength, expectedIntersectionLength, 1e-6*expectedIntersectionLength);
    // EXPECT_DOUBLE_EQ(intersectionLength, expectedIntersectionLength);

    // origin outside crystal, inside crystal
    std::vector<Vector3D> origins{Vector3D(30, 0, 0), Vector3D(2.053, -0.104, 1.034)};
    for (int i = 0; i < origins.size(); i++)
    {
        // load random test data generated by Mathematica
        auto testData = read2DVectorFromFile("/media/ming/DATA/projects/sampler1r2/unittest/testdata/randomDirectionIntersections"+std::to_string(i+1)+".txt", 0, '\t');
        for (int j = 0; j < testData.size(); j++)
        {
            Vector3D direction(testData[j][0], testData[j][1], testData[j][2]);
            double expectedIntersectionLength = testData[j][3];
            double intersectionLength = crystals->getRayIntersectionLength(Ray(origins[i], direction));
            // EXPECT_EQ(intersectionLength, expectedIntersectionLength);
            EXPECT_NEAR(intersectionLength, expectedIntersectionLength, 1e-6);
        }
    }
}

TEST_F(GeometryTest, getEndPoint)
{
    Vector3D expectedEndPoint(0.65, 0, 2);
    Vector3D origin(30, 0, 10);
    Vector3D direction(expectedEndPoint-origin);
    double distance = 0.4664170688955677;
    Vector3D endPoint = crystals->getEndPoint(Ray(origin, direction), distance);
    EXPECT_NEAR(endPoint.x(), expectedEndPoint.x(), 1e-6);
    EXPECT_NEAR(endPoint.y(), expectedEndPoint.y(), 1e-6);
    EXPECT_NEAR(endPoint.z(), expectedEndPoint.z(), 1e-6);
    // EXPECT_DOUBLE_EQ(endPoint.x(), expectedEndPoint.x());
    // EXPECT_DOUBLE_EQ(endPoint.y(), expectedEndPoint.y());
    // EXPECT_DOUBLE_EQ(endPoint.z(), expectedEndPoint.z());

    expectedEndPoint = Vector3D (-0.65, 2.2, -2);
    origin = Vector3D (30, -5, -5);
    direction = Vector3D (expectedEndPoint-origin);
    distance = 0.15478104479008029;
    endPoint = crystals->getEndPoint(Ray(origin, direction), distance);
    EXPECT_NEAR(endPoint.x(), expectedEndPoint.x(), 1e-6);
    EXPECT_NEAR(endPoint.y(), expectedEndPoint.y(), 1e-6);
    EXPECT_NEAR(endPoint.z(), expectedEndPoint.z(), 1e-6);
    // EXPECT_DOUBLE_EQ(endPoint.x(), expectedEndPoint.x());
    // EXPECT_DOUBLE_EQ(endPoint.y(), expectedEndPoint.y());
    // EXPECT_DOUBLE_EQ(endPoint.z(), expectedEndPoint.z());

    // origin outside crystal, inside crystal
    std::vector<Vector3D> origins{Vector3D(30, 0, 0), Vector3D(-30, 5, 5), Vector3D(0.769, 3.380, -1.358)};
    for (int i = 0; i < origins.size(); i++)
    {
        // load random test data generated by Mathematica
        auto testData = read2DVectorFromFile("/media/ming/DATA/projects/sampler1r2/unittest/testdata/randomEndPoints"+std::to_string(i+1)+".txt", 0, '\t');
        for (int j = 0; j < testData.size(); j++)
        {
            Vector3D direction(testData[j][0], testData[j][1], testData[j][2]);
            double length = testData[j][3];
            Vector3D expectedEndPoint(testData[j][4], testData[j][5], testData[j][6]);
            Vector3D endPoint = crystals->getEndPoint(Ray(origins[i], direction), length);
            // EXPECT_DOUBLE_EQ(endPoint.x(), expectedEndPoint.x());
            // EXPECT_DOUBLE_EQ(endPoint.y(), expectedEndPoint.y());
            // EXPECT_DOUBLE_EQ(endPoint.z(), expectedEndPoint.z());
            EXPECT_NEAR(endPoint.x(), expectedEndPoint.x(), 1e-6);
            EXPECT_NEAR(endPoint.y(), expectedEndPoint.y(), 1e-6);
            EXPECT_NEAR(endPoint.z(), expectedEndPoint.z(), 1e-6);
        }
    }
}

TEST_F(GeometryTest, getMaxDistance)
{
    Vector3D origin(30, 0, 0);
    double max_distance = crystals->getMaxDistanceInCrystal(origin);
    EXPECT_GE(max_distance, 1.2);

    // origin outside crystal, inside crystal
    std::vector<Vector3D> origins{Vector3D(30, 0, 0), Vector3D(2.053, -0.104, 1.034)};
    for (int i = 0; i < origins.size(); i++)
    {
        // load random test data generated by Mathematica
        auto testData = read2DVectorFromFile("/media/ming/DATA/projects/sampler1r2/unittest/testdata/randomDirectionIntersections"+std::to_string(i+1)+".txt", 0, '\t');
        double max_of_sampled_distances(0);
        for (int j = 0; j < testData.size(); j++)
        {
            if (max_of_sampled_distances < testData[j][3])
            {
                max_of_sampled_distances = testData[j][3];
            }
        }
        double max_distance = crystals->getMaxDistanceInCrystal(origins[i]);
        EXPECT_GE(max_distance, max_of_sampled_distances);
    }
}