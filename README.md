# Cpp code for sampling two interactions in a series

## Prerequisites
- CMake 3.14 or higher. If CMake is not installed, install it following the instructions [here](https://cmake.org/install/).
- OpenMP for parallelization.

## Compile
```bash
  mkdir build
  cd build
  cmake ..
  cmake --build . --config Release
  ```
## Run Main File
```bash
cd build
./main x0 y0 z0 E0 number_of_samples number_of_threads
```
Six input parameters:
- `x0`, `y0`, `z0`: initial photon position, cm
- `E0`: initial photon eneryg, MeV
- `number_of_samples`: number of samples to generate.
- `number_of_threads`: number of threads to be used. If set to 1, run in serial; if greater than 1, run in parallel using OpenMP.
The output will be saved to a text file `build/sampled_r1r2E1E2.txt`.
## Sources
- `main.cpp`: Main file for execution.
- `srcf/`:
    - `geometry.cpp`: Geometric calculations, such as finding intersections of ray and crystals.
    - `material.cpp`: Generate look-up table of attenuation coefficients.
    - `sample.cpp`: Sample two interaction positions $r_1, r_2$, and two deposited energies $E_1, E_2$ in the imager.

## Timing
With 10 threads, it took approximately 0.6s to generate 100k coincidence events. Measured on an Intel Core i9-7920X CPU @ 2.90GHz × 24.

## Notes
Please see this [document](https://www.overleaf.com/read/dpqgzmzjfjhs).
