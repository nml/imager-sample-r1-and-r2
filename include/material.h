#pragma once

#include <vector>
#include <cmath>
#include "util.h"

class Material
{
private:
    double _density;
    std::vector<double> _energyGrid;
    double _minEnergy;
    double _maxEnergy;
    const int _nPoints{100};
    double _deltaEnergy;
    std::vector<double> _ComptonScatteringProbability;
    std::vector<double> _totalAttenCoeff;

    // bool _readTable(const std::string fpath, std::vector<std::vector<double>>& table);
    std::vector<double> _linearInterpolation(const std::vector<double>& newxs, const std::vector<double>& xs, const std::vector<double>& ys);
    bool _createLookupTable(const std::vector<std::vector<double>>& table);
    int  _getEnergyIndex(const double energy) const;
public:
    Material(const std::string fpath, const double rho);

    double getTotalAttenCoeff(const double energy) const;

    double getComptonScatteringProbability(const double energy) const;
};
