#pragma once

#include <random>
#include <tuple>
#include <vector>
#include <string>
#include <fstream>
#include <iomanip> 
#include <omp.h>
#include "material.h"
#include "geometry.h"


struct TwoEvents
{
    Vector3D r1;
    Vector3D r2;
    double E1;
    double E2;
};


class R1R2Sampler
{
private:
    Material _lyso;
    Crystals _crystals;

    std::default_random_engine _generator;
    std::uniform_real_distribution<double> _distribution;
    double randomReal() {return _distribution(_generator);}

    std::pair<double, double> _sampleMuPhiInRange(const std::vector<std::vector<std::pair<double, double>>>& mu_phi_ranges);
    Vector3D _sampleR1Direction(const Vector3D& r0, const double E0);
    std::pair<double, double> _sampleComptonScatteringAngle(const double energy);
    Vector3D _sampleR2Direction(const Vector3D& r0, const double E0, const Vector3D& r1, double& scattered_photon_energy);
    double _sampleDistanceInCrystal(const double dmax, const double energy);
    Vector3D _sampleNextInteractionPosition(const Vector3D& r, const Vector3D& direction, const double energy);
    Vector3D _sampleR1(const Vector3D& r0, const double E0);
    Vector3D _sampleR2E1(const Vector3D& r0, const double E0, const Vector3D& r1, double& E1);
    double _sampleE2(const double E0, const double E1);

public:
    R1R2Sampler() : _lyso("LYSO_atten_coeff.txt", 7.25) {};
    R1R2Sampler(int seed) : _generator(seed), _lyso("LYSO_atten_coeff.txt", 7.25) {};

    TwoEvents sampleR1R2E1E2(const Vector3D& r0, const double E0);
};

std::vector<TwoEvents> worker(const Vector3D& r0, const double E0, const int nSamplePerWorker, const int seed=-1);
std::vector<TwoEvents> run(const Vector3D& r0, const double E0, const int nSample, const int numWorkers=1);
void saveEvents2txt(const std::string fpath, const std::vector<TwoEvents>& results);