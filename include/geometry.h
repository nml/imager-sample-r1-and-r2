#pragma once

#include <vector>
#include <algorithm>
#include <stdexcept>
#include "util.h"

class Ray
{
private:
public:
    Ray(const Vector3D origin_, const Vector3D direction_)
     : origin(origin_), direction(direction_.normalized())
    {
    }

    Vector3D origin;
    Vector3D direction;

    Vector3D getCoordinate(double t) const {return origin + t * direction;}

    double getXPlaneIntersection(double x) const {return (x-origin.x())/direction.x();}

    double getYPlaneIntersection(double y) const {return (y-origin.y())/direction.y();}

    double getZPlaneIntersection(double z) const {return (z-origin.z())/direction.z();}
};


class Crystals
{
private:
    const double _crystalSideLength{0.3};  // crystal size, cm
    const double _crystalHeight{5};  // crystal height, cm
    const double _pitchX{1.3};  // pitch x, cm
    const double _pitchY{1.1};  // pitch y, cm
    const int _numberX{4};  // columns
    const int _numberY{7};  // rows
    std::vector<double> _xPlanes;
    std::vector<double> _yPlanes;
    double _xmin;
    double _xmax;
    double _ymin;
    double _ymax;
    double _zmin{-_crystalHeight / 2};
    double _zmax{_crystalHeight / 2};
    std::vector<double> _extents;
    std::vector<double> _xCenters;
    std::vector<double> _yCenters;
    // vertices
    std::vector<std::vector<Vector3D>> _vertices;

    bool _checkXInCrystal(const double x) const;
    bool _checkYInCrystal(const double x) const;
    // std::vector<std::pair<double, double>> _getYPlaneIntersections(const Ray& ray) const;
    // std::vector<std::pair<double, double>> _getXPlaneIntersections(const Ray& ray) const;
    std::vector<std::pair<double, double>> _getXYPlaneIntersections(const Ray& ray, const int case_xy) const;
    std::vector<std::pair<double, double>> _getYZRayIntersections(const Ray& ray) const;
    std::vector<std::pair<double, double>> _getXZRayIntersections(const Ray& ray) const;
    std::vector<std::pair<double, double>> _getZRayIntersections(const Ray& ray) const;
    std::vector<std::pair<double, double>> _getIntersections(const Ray& ray) const;

public:
    Crystals();

    bool _checkIntersectBoundingBox(const Ray& ray) const;
    double getRayIntersectionLength(const Ray& ray) const;
    Vector3D getEndPoint(const Ray& ray, const double d) const;
    std::vector<std::vector<std::pair<double, double>>> getMuPhiRanges(const Vector3D& r0) const;
    double getMaxDistanceInCrystal(const Vector3D& r0) const;
};
