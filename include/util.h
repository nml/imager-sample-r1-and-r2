#pragma once


#include <cmath>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>

class Vector3D
{
private:
    /* data */
    double X;
    double Y;
    double Z;
public:
    Vector3D():Vector3D(0, 0, 0) {}
    Vector3D(double x_, double y_, double z_)
        : X(x_), Y(y_), Z(z_)
        {}
    
    void setX(double x) {X=x;}
    void setY(double y) {Y=y;}
    void setZ(double z) {Z=z;}

    double x() const {return X;}
    double y() const {return Y;}
    double z() const {return Z;}
    double length() const {return std::sqrt(X*X+Y*Y+Z*Z);}
    double lengthSquared() const {return X*X+Y*Y+Z*Z;}
    void normalize() {double norm = length(); X=X/norm;Y=Y/norm;Z=Z/norm;}
    Vector3D normalized() const {double norm = length(); return Vector3D(X/norm, Y/norm, Z/norm);}
    double distanceToLine(const Vector3D &point, const Vector3D &direction) const
    {
        Vector3D delta = point - *this;
        return std::sqrt(delta.lengthSquared() * direction.lengthSquared() - std::pow(delta*direction, 2))/direction.length();
    }

    Vector3D operator+(const Vector3D& r) const {
        return Vector3D(this->X + r.X, this->Y + r.Y, this->Z + r.Z);
    }
    Vector3D operator-(const Vector3D& r) const {
        return Vector3D(this->X - r.X, this->Y - r.Y, this->Z - r.Z);
    }

    double operator*(const Vector3D& r) const {
        return this->X * r.X + this->Y * r.Y + this->Z * r.Z;
    }
    Vector3D operator/(const double d) const {
        return Vector3D(this->X / d,  this->Y / d, this->Z / d);
    }
    Vector3D operator-() const {
        return Vector3D(-this->X, -this->Y, -this->Z);
    }

    Vector3D& operator*=(double d) {X*=d; Y*=d; Z*=d; return *this; }
    Vector3D& operator*=(const Vector3D& vector) {X*=vector.X; Y*=vector.Y; Z*=vector.Z; return *this; }
    // Vector3D& operator+=(double d) {X+=d; Y+=d; Z+=d; calculateNorm(); return *this; }
    Vector3D& operator+=(const Vector3D& vector) {X+=vector.X; Y+=vector.Y; Z+=vector.Z; return *this; }
    // Vector3D& operator-=(double d) {X-=d; Y-=d; Z-=d; calculateNorm(); return *this; }
    Vector3D& operator-=(const Vector3D& vector) {X-=vector.X; Y-=vector.Y; Z-=vector.Z; return *this; }
    Vector3D& operator/=(double d) {X/=d; Y/=d; Z/=d; return *this; }
    Vector3D& operator/=(const Vector3D& vector) {X/=vector.X; Y/=vector.Y; Z/=vector.Z; return *this; }

    static double dotProduct(const Vector3D& v1, const Vector3D& v2) {return v1*v2;}

    static Vector3D crossProduct(const Vector3D& v1, const Vector3D& v2) 
    {
        double x = v1.y() * v2.z() - v1.z() * v2.y();
        double y = -v1.x() * v2.z() + v1.z() * v2.x();
        double z = v1.x() * v2.y() - v1.y() * v2.x();
        return Vector3D(x,y,z);
    }
};

inline bool	operator!=(const Vector3D &v1, const Vector3D &v2) {
    return v1.x() != v2.x() || v1.y() != v2.y() || v1.z() != v2.z();
}
inline bool	operator==(const Vector3D &v1, const Vector3D &v2) {
    return v1.x() == v2.x() && v1.y() == v2.y() && v1.z() == v2.z();
}

inline Vector3D operator*(double d, const Vector3D& vector) {
    return Vector3D(vector.x() * d,  vector.y() * d, vector.z() * d);
}
inline Vector3D operator*(const Vector3D& vector, double d)
{
    return Vector3D(vector.x() * d,  vector.y() * d, vector.z() * d);
}

inline std::vector<std::vector<double>> read2DVectorFromFile(const std::string fpath, const int skiprows=0, const char delimiter=' ')
{
    std::vector<std::vector<double>> table;
    std::ifstream fileptr;
    fileptr.open(fpath, std::ios::in);
    if (!fileptr.is_open())
    {
        std::string errMessage = "can't open file: " + fpath;
        throw std::runtime_error(errMessage);
    }
    std::string line;
    // skip dummy lines
    for (int i = 0; i < skiprows; i++)
    {
        std::getline(fileptr, line);
    }
    // Read one line at a time into the variable line
    while(std::getline(fileptr, line))
    {
        std::stringstream lineStream(line);
        std::string word;
        std::vector<double> row;
        while(std::getline(lineStream, word, delimiter))
        {
            row.push_back(std::stod(word));
        }
        table.push_back(row);
    }
    return table;
}

template <class T>
inline void transpose(std::vector<std::vector<T>> &b)
{
    if (b.size() == 0)
        return;

    std::vector<std::vector<T>> trans_vec(b[0].size(), std::vector<T>());

    for (int i = 0; i < b.size(); i++)
    {
        for (int j = 0; j < b[i].size(); j++)
        {
            trans_vec[j].push_back(b[i][j]);
        }
    }
    b = trans_vec;
}