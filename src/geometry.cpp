#include "geometry.h"


Crystals::Crystals()
{
    // initialize _xPlanes, _yPlanes
    // initialize xCenters and yCenters
    for (int i = 0; i < _numberX; i++)
    {
        _xPlanes.push_back(-1.95+_pitchX*i-_crystalSideLength/2);
        _xPlanes.push_back(-1.95+_pitchX*i+_crystalSideLength/2);
        _xCenters.push_back(-1.95+_pitchX*i);
    }
    for (int i = 0; i < _numberY; i++)
    {
        _yPlanes.push_back(-3.3+_pitchY*i-_crystalSideLength/2);
        _yPlanes.push_back(-3.3+_pitchY*i+_crystalSideLength/2);
        _yCenters.push_back(-3.3+_pitchY*i);
    }
    // intialize min & max & extents
    _xmin = _xPlanes[0];
    _xmax = _xPlanes[_xPlanes.size()-1];
    _ymin = _yPlanes[0];
    _ymax = _yPlanes[_yPlanes.size()-1];
    _extents = std::vector<double>{_xmax, _ymax, _zmax};
    // initialize vertices
    for (int i = 0; i < _numberX; i++)
    {
        for (int j = 0; j < _numberY; j++)
        {
            std::vector<Vector3D> oneCrystalVertices;
            for (int k = 0; k < 2; k++)
            {
                for (int l = 0; l < 2; l++)
                {
                    oneCrystalVertices.push_back(Vector3D(_xPlanes[2*i+k], _yPlanes[2*j+l], _zmax));
                }
            }
            for (int k = 0; k < 2; k++)
            {
                for (int l = 0; l < 2; l++)
                {
                    oneCrystalVertices.push_back(Vector3D(_xPlanes[2*i+k], _yPlanes[2*j+l], _zmin));
                }
            }
            _vertices.push_back(oneCrystalVertices);
        }
    }
}

bool Crystals::_checkXInCrystal(const double x) const
{
    int i = std::rint((x-_xCenters[0])/_pitchX);
    if (i<0 || i>=_numberX)
        return false;
    double dx = x - _xCenters[i];
    if (std::abs(dx) > _crystalSideLength)
        return false;
    return true;    
}

bool Crystals::_checkYInCrystal(const double y) const
{
    int j = std::rint((y-_yCenters[0])/_pitchY);
    if (j<0 || j>=_numberY)
        return false;
    double dy = y - _yCenters[j];
    if (std::abs(dy) > _crystalSideLength)
        return false;
    return true;
}

bool Crystals::_checkIntersectBoundingBox(const Ray& ray) const
{
    if (std::abs(ray.origin.x()) > _extents[0] && ray.direction.x() * ray.origin.x() > 0)
        return false;
    if (std::abs(ray.origin.y()) > _extents[1] && ray.direction.y() * ray.origin.y() > 0)
        return false;
    if (std::abs(ray.origin.z()) > _extents[2] && ray.direction.z() * ray.origin.z() > 0)
        return false;
    Vector3D DxO = Vector3D::crossProduct(ray.direction, ray.origin);
    if (std::abs(DxO.x()) > _extents[1] * std::abs(ray.direction.z()) + _extents[2] * std::abs(ray.direction.y()))
        return false;
    if (std::abs(DxO.y()) > _extents[0] * std::abs(ray.direction.z()) + _extents[2] * std::abs(ray.direction.x()))
        return false;
    if (std::abs(DxO.z()) > _extents[0] * std::abs(ray.direction.y()) + _extents[1] * std::abs(ray.direction.x()))
        return false;
    return true;
}

std::vector<std::pair<double, double>> Crystals::_getXYPlaneIntersections(const Ray& ray, 
                  const int case_xy) const
{
    std::vector<double> ts;
    if (case_xy==0)
    {
        for (auto x_j : _xPlanes)
        {
            double t;
            t = ray.getXPlaneIntersection(x_j);
            Vector3D p = ray.getCoordinate(t);
            if (p.z() > _zmax)
            {
                ts.push_back(ray.getZPlaneIntersection(_zmax));
            }
            else if (p.z() < _zmin)
            {
                ts.push_back(ray.getZPlaneIntersection(_zmin));
            }
            else
            {
                ts.push_back(t);
            }
        }
    }
    else
    {
        for (auto y_j : _yPlanes)
        {
            double t;
            t = ray.getYPlaneIntersection(y_j);
            Vector3D p = ray.getCoordinate(t);
            if (p.z() > _zmax)
            {
                ts.push_back(ray.getZPlaneIntersection(_zmax));
            }
            else if (p.z() < _zmin)
            {
                ts.push_back(ray.getZPlaneIntersection(_zmin));
            }
            else
            {
                ts.push_back(t);
            }
        }
    }
    
    std::vector<std::pair<double, double>> intervals;
    for (int j = 0; j < ts.size()/2; j++)
    {
        if (ts[2*j] < ts[2*j+1] && ts[2*j+1] > 0)
        {
            double t_left = ts[2*j]>0 ? ts[2*j] : 0;
            intervals.push_back(std::make_pair(t_left, ts[2*j+1]));
        }
        else if (ts[2*j] > ts[2*j+1] && ts[2*j] > 0)
        {
            double t_left = ts[2*j+1]>0 ? ts[2*j+1] : 0;
            intervals.push_back(std::make_pair(t_left, ts[2*j]));
        }
    }
    return intervals;
}

// std::vector<std::pair<double, double>> Crystals::_getXPlaneIntersections(const Ray& ray) const
// {}
std::vector<std::pair<double, double>> Crystals::_getYZRayIntersections(const Ray& ray) const
{
    if (!_checkXInCrystal(ray.origin.x()))
    {
        return {};
    }
    if (std::abs(ray.direction.y()) < 1e-4)
    {
        return _getZRayIntersections(ray);
    }
    return _getXYPlaneIntersections(ray, 1);
}

std::vector<std::pair<double, double>> Crystals::_getXZRayIntersections(const Ray& ray) const
{
    if (!_checkYInCrystal(ray.origin.y()))
    {
        return {};
    }
    if (std::abs(ray.direction.x()) < 1e-4)
    {
        return _getZRayIntersections(ray);
    }
    return _getXYPlaneIntersections(ray, 0);
}

std::vector<std::pair<double, double>> Crystals::_getZRayIntersections(const Ray& ray) const
{
    if (!_checkXInCrystal(ray.origin.x()))
    {
        return {};
    }
    if (!_checkYInCrystal(ray.origin.y()))
    {
        return {};
    }
    
    if (std::abs(ray.origin.z()) < _zmax)
    {
        if (ray.direction.z() > 0)
            return {std::make_pair(0, _zmax - ray.origin.z())};
        else
            return {std::make_pair(0, ray.origin.z()-_zmin)};
        
    }
    else
    {
        if (ray.direction.z() > 0)
            return {std::make_pair(_zmin - ray.origin.z(), _zmax - ray.origin.z())};
        else
            return {std::make_pair(ray.origin.z()-_zmax, ray.origin.z()-_zmin)};
    }
}
std::vector<std::pair<double, double>> Crystals::_getIntersections(const Ray& ray) const
{
    if (!_checkIntersectBoundingBox(ray))
    {
        return {};
    }
    if (std::abs(ray.direction.x()) < 1e-4)
    {
        return _getYZRayIntersections(ray);
    }
    if (std::abs(ray.direction.y()) < 1e-4)
    {
        return _getXZRayIntersections(ray);
    }

    std::vector<std::pair<double, double>> intervals;
    std::vector<std::pair<double, double>> xPlaneIntervals = _getXYPlaneIntersections(ray, 0);
    std::vector<std::pair<double, double>> yPlneIntervals = _getXYPlaneIntersections(ray, 1);
    for (auto &&int_i : xPlaneIntervals)
    {
        for (auto &&int_j : yPlneIntervals)
        {
            if (int_j.first >= int_i.second || int_j.second <= int_i.first)
            {
                continue;
            }
            double t_left = (int_i.first > int_j.first) ? int_i.first : int_j.first;
            double t_right = (int_i.second < int_j.second) ? int_i.second : int_j.second;
            intervals.push_back(std::make_pair(t_left, t_right));
        }
    }
    return intervals;
}

double Crystals::getRayIntersectionLength(const Ray& ray) const
{
    std::vector<std::pair<double, double>> intervals = _getIntersections(ray);
    if (intervals.size() == 0)
    {
        return 0;
    }
    double s(0);
    for (auto &&int_i : intervals)
    {
        s += int_i.second - int_i.first;
    }
    return s;
}
Vector3D Crystals::getEndPoint(const Ray& ray, const double d) const
{
    std::vector<std::pair<double, double>> intervals = _getIntersections(ray);
    if (intervals.size() == 0)
    {
        throw std::runtime_error("No intersecions found.");
    }
    std::sort(intervals.begin(), intervals.end());
    double s(d);
    for (auto &&int_i : intervals)
    {
        s -= int_i.second - int_i.first;
        if (s <= 0)
        {
            return ray.getCoordinate(int_i.second+s);
        }
    }
}
std::vector<std::vector<std::pair<double, double>>> Crystals::getMuPhiRanges(const Vector3D& r0) const
{
    bool flag(false);
    std::vector<std::vector<std::pair<double, double>>> mu_phi_ranges;
    for (auto &&v : _vertices)
    {
        double x_left = v[0].x();
        double x_right = v[2].x();
        double y_bottom = v[0].y();
        double y_top = v[1].y();
        std::pair<double, double> phis_j;
        if (r0.x() > x_left && r0.x() < x_right && r0.y() > y_bottom && r0.y() < y_top)
        {
            phis_j = {0, M_PI*2};
        }
        else if (r0.x() >= x_right && r0.y() > y_bottom && r0.y() < y_top)
        {
            flag = true;
            phis_j = {std::atan2(y_top-r0.y(), x_right-r0.x()), std::atan2(y_bottom-r0.y(), x_right-r0.x())+2*M_PI};
        }
        else
        {
            double phi_j_min(10);
            double phi_j_max(-10);
            for (int i = 0; i < 4; i++)
            {
                double phi_j_vertice_i = std::atan2(v[i].y()-r0.y(), v[i].x()-r0.x());
                if (phi_j_min > phi_j_vertice_i)
                {
                    phi_j_min = phi_j_vertice_i;
                }
                if (phi_j_max < phi_j_vertice_i)
                {
                    phi_j_max = phi_j_vertice_i;
                }
            }
            phis_j = {phi_j_min, phi_j_max};
        }
        double mu_j_min(1);
        double mu_j_max(-1);
        for (auto &&p : v)
        {
            double mu_j_vertice_p = (p.z()-r0.z()) / ((p-r0).length());
            if (mu_j_min > mu_j_vertice_p)
            {
                mu_j_min = mu_j_vertice_p;
            }
            if (mu_j_max < mu_j_vertice_p)
            {
                mu_j_max = mu_j_vertice_p;
            }
        }
        std::pair<double, double> mus_j{mu_j_min, mu_j_max};
        mu_phi_ranges.push_back({mus_j, phis_j});
    }
    if (flag)
    {
        for (auto &&mu_phi_j : mu_phi_ranges)
        {
            if (mu_phi_j[1].first < 0 || mu_phi_j[1].second < 0)
            {
                mu_phi_j[1].first += 2*M_PI;
                mu_phi_j[1].second += 2*M_PI;
            }
        }
    }
    return mu_phi_ranges;
}
double Crystals::getMaxDistanceInCrystal(const Vector3D& r0) const
{
    double max_distance(-1);
    int shift = (r0.z()>0)? 4 : 0;
    for (auto &&v : _vertices)
    {
        for (int i = shift; i < 4+shift; i++)
        {
            Vector3D omega_i = v[i]-r0;
            Ray ray_i(r0, omega_i);
            double distance_to_vertice_i = getRayIntersectionLength(ray_i);
            if (max_distance < distance_to_vertice_i)
            {
                max_distance = distance_to_vertice_i;
            }
        }
    }
    return max_distance;
}