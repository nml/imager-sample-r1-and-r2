#include "material.h"


Material::Material(const std::string fpath, const double rho)
 : _density(rho)
{
    std::vector<std::vector<double>> coarseTable = read2DVectorFromFile(fpath, 3);
    transpose(coarseTable);
    // _readTable(fpath, coarseTable);
    _minEnergy = coarseTable[0][0];
    _maxEnergy = coarseTable[0][coarseTable[0].size()-1];
    _createLookupTable(coarseTable);
}


double Material::getTotalAttenCoeff(const double energy) const
{
    return _totalAttenCoeff[_getEnergyIndex(energy)];
}

double Material::getComptonScatteringProbability(const double energy) const
{
    return _ComptonScatteringProbability[_getEnergyIndex(energy)];
}

bool Material::_createLookupTable(const std::vector<std::vector<double>>& table)
{
    _deltaEnergy = (_maxEnergy - _minEnergy) / (_nPoints-1);
    for (int i = 0; i < _nPoints; i++)
    {
        _energyGrid.push_back(_minEnergy + i * _deltaEnergy);
    }
    std::vector<double> ComptonScatteringCrossSection = _linearInterpolation(_energyGrid, table[0], table[1]);
    std::vector<double> absorptionCrossSection = _linearInterpolation(_energyGrid, table[0], table[2]);
    for (int i = 0; i < _energyGrid.size(); i++)
    {
        _ComptonScatteringProbability.push_back(ComptonScatteringCrossSection[i] / (ComptonScatteringCrossSection[i]+absorptionCrossSection[i]));
    }
    
    _totalAttenCoeff = _linearInterpolation(_energyGrid, table[0], table[3]);
    for (int i = 0; i < _totalAttenCoeff.size(); i++)
    {
        _totalAttenCoeff[i] *= _density;
    }
    return true;
}

std::vector<double> Material::_linearInterpolation(const std::vector<double>& newxs, 
                                                   const std::vector<double>& xs, 
                                                   const std::vector<double>& ys)
{
    std::vector<double> newys(newxs.size(), 0);
    int j_last(0);
    for (int i = 0; i < newxs.size(); i++)
    {
        if (newxs[i] <= _minEnergy)
            newys[i] = ys[0];
        else if (newxs[i] >= _maxEnergy)
            newys[i] = ys[ys.size()-1];
        else
        {
            for (int j = j_last; j < ys.size() - 1; j++)
            {
                if (xs[j] <= newxs[i] && newxs[i] < xs[j+1])
                {
                    double newy = ((newxs[i] - xs[j]) * ys[j+1] + (xs[j+1] - newxs[i]) * ys[j]) / (xs[j+1] - xs[j]);
                    newys[i] = newy;
                    j_last = j;
                    break;
                }
            }
        }   
    }
    return newys;
}

int Material::_getEnergyIndex(const double energy) const
{
    if (energy <= _minEnergy)
        return 0;
    else if (energy >= _maxEnergy)
        return _energyGrid.size() - 1;
    else
    {
        return std::lrint((energy - _minEnergy)/_deltaEnergy);
    }
}