#include "sampling.h"

std::pair<double, double> R1R2Sampler::_sampleMuPhiInRange(const std::vector<std::vector<std::pair<double, double>>>& mu_phi_ranges)
{
    double mu_min(1), mu_max(-1), phi_min(10), phi_max(-10);
    for (auto &&mu_phi_range : mu_phi_ranges)
    {
        const auto& mu_range = mu_phi_range[0];
        const auto& phi_range = mu_phi_range[1];
        if (mu_min > mu_range.first)
        {
            mu_min = mu_range.first;
        }
        if (mu_max < mu_range.second)
        {
            mu_max = mu_range.second;
        }
        if (phi_min > phi_range.first)
        {
            phi_min = phi_range.first;
        }
        if (phi_max < phi_range.second)
        {
            phi_max = phi_range.second;
        }
    }
    bool in_range(false);
    double mu, phi;
    while (!in_range)
    {
        double R1(randomReal());
        double R2(randomReal());
        mu = mu_min + R1 * (mu_max - mu_min);
        phi = phi_min + R2 * (phi_max - phi_min);
        for (auto &&mu_phi_range : mu_phi_ranges)
        {
            const auto& mu_range = mu_phi_range[0];
            const auto& phi_range = mu_phi_range[1];
            if (mu > mu_range.first && mu < mu_range.second && phi > phi_range.first && phi < phi_range.second)
            {
                in_range = true;
                break;
            }
        }
    }
    return {mu, phi};
}

Vector3D R1R2Sampler::_sampleR1Direction(const Vector3D& r0, const double E0) 
{
    double atten_coeff = _lyso.getTotalAttenCoeff(E0);
    auto mu_phi_ranges = _crystals.getMuPhiRanges(r0);
    double max_distance = _crystals.getMaxDistanceInCrystal(r0);
    bool accepted(false);
    Vector3D direction;
    while (!accepted)
    {
        double mu, phi;
        std::tie(mu, phi) = _sampleMuPhiInRange(mu_phi_ranges);
        double sin_theta = std::sqrt(1-mu*mu);
        direction = Vector3D(sin_theta * std::cos(phi), sin_theta*std::sin(phi), mu);
        double distance = _crystals.getRayIntersectionLength(Ray(r0, direction));
        double ratio = (1-std::exp(-atten_coeff*distance)) / (1-std::exp(-atten_coeff*max_distance));
        if (randomReal() <= ratio)
        {
            accepted = true;
        }
    }
    return direction;
}

std::pair<double, double> R1R2Sampler::_sampleComptonScatteringAngle(const double energy) 
{
    double alpha(energy/0.511);
    double R1, R2, R3;
    double eta;
    double cosAng;
    while (1)
    {
        R1 = randomReal();
        R2 = randomReal();
        R3 = randomReal();
        if((2*alpha+9)* R1 <= (2*alpha + 1))
        {
            eta = 1 + 2 * alpha * R2;
            if (R3*eta*eta <= 4*(eta-1))
            {
                cosAng = 1 - 2 * R2;
                break;
            }
            else
            {
                continue;
            }
        }
        else
        {
            eta = (2*alpha+1)/(2*R2*alpha+1);
            cosAng = 1-(eta-1)/alpha;
            if(R3 <= 0.5*(cosAng * cosAng + 1/ eta))
            {
                break;
            }
            else
            {
                continue;
            }
        }
    }
    return {cosAng, eta};
}

Vector3D R1R2Sampler::_sampleR2Direction(const Vector3D& r0, const double E0, const Vector3D& r1, double& scattered_photon_energy) 
{
    double cosAng, eta;
    std::tie(cosAng, eta) = _sampleComptonScatteringAngle(E0);
    scattered_photon_energy = E0/eta;
    double phi = 2*M_PI*randomReal();

    Vector3D dir = r1-r0;
    dir.normalize();
    double R1 = dir.x();
    double R2 = dir.y();
    double R3 = dir.z();
    double sinAng = std::sqrt(1 - cosAng * cosAng);
    if (std::abs(std::abs(R3) - 1) > 1e-8)
    {
        double eta = 1.0 / std::sqrt(1 - R3 * R3);
        dir.setX(cosAng * R1 + sinAng * (std::cos(phi) * R3 * R1 - std::sin(phi) * R2) * eta);
        dir.setY(cosAng * R2 + sinAng * (std::cos(phi) * R3 * R2 + std::sin(phi) * R1) * eta);
        dir.setZ(cosAng * R3 - sinAng * std::cos(phi) / eta);
    }
    else
    {
        dir.setX(sinAng * std::cos(phi));
        dir.setY(sinAng * std::sin(phi));
        dir.setZ(cosAng * R3);
    }
    return dir;
}

double R1R2Sampler::_sampleDistanceInCrystal(const double dmax, const double energy) 
{
    double atten_coeff = _lyso.getTotalAttenCoeff(energy);
    return -std::log(1-randomReal()*(1-std::exp(-atten_coeff*dmax))) / atten_coeff;
}

Vector3D R1R2Sampler::_sampleNextInteractionPosition(const Vector3D& r, const Vector3D& direction, const double energy) 
{
    Ray ray(r, direction);
    double max_distance_in_crystal = _crystals.getRayIntersectionLength(ray);
    double sampled_distance_in_crystal = _sampleDistanceInCrystal(max_distance_in_crystal, energy);
    return _crystals.getEndPoint(ray, sampled_distance_in_crystal);
}

Vector3D R1R2Sampler::_sampleR1(const Vector3D& r0, const double E0) 
{
    Vector3D omega_1 = _sampleR1Direction(r0, E0);
    return _sampleNextInteractionPosition(r0, omega_1, E0);
}

Vector3D R1R2Sampler::_sampleR2E1(const Vector3D& r0, const double E0, const Vector3D& r1, double& E1) 
{
    double photon_energy_1;
    Vector3D omega_2 = _sampleR2Direction(r0, E0, r1, photon_energy_1);
    Vector3D sampled_r2 = _sampleNextInteractionPosition(r1, omega_2, photon_energy_1);
    E1 = E0 - photon_energy_1;
    return sampled_r2;
}

double R1R2Sampler::_sampleE2(const double E0, const double E1) 
{
    double energy(E0-E1);
    if (randomReal() < _lyso.getComptonScatteringProbability(energy))
    {
        double cosAng, eta;
        std::tie(cosAng, eta) = _sampleComptonScatteringAngle(energy);
        return energy * (1-1/eta);
    }
    return energy;
}

TwoEvents R1R2Sampler::sampleR1R2E1E2(const Vector3D& r0, const double E0) 
{
    TwoEvents returnValue;
    returnValue.r1 = _sampleR1(r0, E0);
    returnValue.r2 = _sampleR2E1(r0, E0, returnValue.r1, returnValue.E1);
    returnValue.E2 = _sampleE2(E0, returnValue.E1);
    return returnValue;
}

std::vector<TwoEvents> worker(const Vector3D& r0, const double E0, const int nSamplePerWorker, const int seed)
{
    R1R2Sampler* sampler;
    if (seed != -1)
    {
        sampler = new R1R2Sampler(seed);
    }
    else
    {
        sampler = new R1R2Sampler();
    }
    std::vector<TwoEvents> sampled_events;
    for (int i = 0; i < nSamplePerWorker; i++)
    {
        sampled_events.push_back(sampler->sampleR1R2E1E2(r0, E0));
    }
    delete sampler;
    return sampled_events;
}

std::vector<TwoEvents> run(const Vector3D& r0, const double E0, const int nSample, const int numWorkers)
{
    std::vector<TwoEvents> results;
    if (numWorkers == 1)
    {
        // serial
        results = worker(r0, E0, nSample);
    }
    else if (numWorkers > 1)
    {
        // generate seeds
        int max_num_threads = omp_get_max_threads();
        std::vector<int> seeds(max_num_threads, 0);
        for (int i = 0; i < max_num_threads; i++)
        {
            seeds[i] = rand();
        }
        const int nSamplePerWorker = nSample / numWorkers;
        omp_set_num_threads(numWorkers);
        #pragma omp parallel shared(seeds)
        {
            std::vector<TwoEvents> results_private;
            #pragma omp for nowait
            for (int i=0;i<numWorkers;i++)
            {
                results_private = worker(r0, E0, nSamplePerWorker, seeds[i]);
            }
            #pragma omp critical
            results.insert(results.end(), results_private.begin(), results_private.end());
        }
    }
    return results;
}

void saveEvents2txt(const std::string fpath, const std::vector<TwoEvents>& results)
{
    std::ofstream fileptr(fpath, std::ios::out);
    if (!fileptr.is_open())
    {
        std::string errMessage = "can't open file: " + fpath;
        throw std::runtime_error(errMessage);
    }
    fileptr << "r1_x\t r1_y\t r1_z\t r2_x\t r2_y\t r2_z\t E1\t E2\n";
    fileptr << std::fixed << std::setprecision(8);
    for (int i = 0; i < results.size(); i++)
    {
        fileptr << results[i].r1.x() << '\t' << results[i].r1.y() << '\t' << results[i].r1.z() << '\t' 
                << results[i].r2.x() << '\t' << results[i].r2.y() << '\t' << results[i].r2.z() << '\t' 
                << results[i].E1     << '\t' << results[i].E2     << '\n';
    }
    fileptr.close();
}