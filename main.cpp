#include <chrono>
#include <iostream>
#include "sampling.h"

int main(int argc, char** argv)
{
    if (argc < 7)
    {
        std::cerr << "Too few arguments. Six parameters: r0_x, r0_y, r0_z, E0, number of samples and number of threads, are needed." << std::endl;
        exit(-1);
    }
    
    const Vector3D r0(std::stod(argv[1]), std::stod(argv[2]), std::stod(argv[3])); // initial position, cm.
    const double E0(std::stod(argv[4])); // initial energy, MeV.

    const int nSample(std::stoi(argv[5])); // number of samples to generate.
    const int numWorkers(std::stoi(argv[6])); // number of threads will be used. if set to 1, run in serial.

    auto startTime = std::chrono::high_resolution_clock::now();
    auto sampled_r1r2E1E2 = run(r0, E0, nSample, numWorkers);
    auto endTime = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count() << "ms" << std::endl;
    
    saveEvents2txt("sampled_r1r2E1E2.txt", sampled_r1r2E1E2); // save events to a text file.
    
    return 0;
}